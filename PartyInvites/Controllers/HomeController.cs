﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace PartyInvites.Controllers
{
    public class HomeController : Controller
    {
        public ViewResult Index()
        {
            return View();
        }

        public ViewResult AboutUs()
        {
            string aboutinfo = "about us";
            ViewBag.AboutInfo = aboutinfo;
            return View();
        }

        public ViewResult ControllerViewPractice() 
        {
            return View();
        }

        public ViewResult MyView()
        {
            int hour = DateTime.Now.Hour;
            int minute = DateTime.Now.Minute;
            DayOfWeek dayToday = DateTime.Now.DayOfWeek;
            ViewBag.TimeHour = hour;
            ViewBag.TimeMinute = minute;
            ViewBag.TimeDayToday = dayToday;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Evening";
            return View();
        }

        public ViewResult Privacy()
        {
            return View();
        }
    }
}
 